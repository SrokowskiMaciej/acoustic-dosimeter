package com.acousticdosimeter.Utilities;

import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorRes;

/**
 * Utility class for manipulating color resources.
 * Created by maciek on 21.01.16.
 */
public class ColorUtils {

    /**
     * Gets color integer based on color resource. API version independent.
     *
     * @param context Context
     * @param id      ResourceId
     * @return color in integer value
     */
    public static int getColor(Context context, @ColorRes int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(id);
        } else {
            //noinspection deprecation
            return context.getResources().getColor(id);
        }
    }
}
