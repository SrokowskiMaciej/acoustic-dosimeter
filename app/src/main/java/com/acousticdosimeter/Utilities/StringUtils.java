package com.acousticdosimeter.Utilities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import java.util.concurrent.TimeUnit;

/**
 * Utility class used for formatting strings
 * Created by maciek on 21.01.16.
 */
public class StringUtils {

    private static final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();

    /**
     * Used for converting time in milliseconds to human readable form.
     *
     * @param timeMillisec time in milliseconds
     * @return String formated as minutes and seconds.
     */
    public static String getFormattedTime(long timeMillisec) {
        return String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(timeMillisec),
                TimeUnit.MILLISECONDS.toSeconds(timeMillisec) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeMillisec))
        );
    }

    /**
     * Builds spannable string from given {@link ColoredString} objects based on their text and color resource
     * assigned to every one of them.
     *
     * @param context        Context
     * @param coloredStrings Colored String
     * @return CharSequence with colors assigned.
     */
    public static CharSequence getSpannableString(@NonNull Context context, @NonNull ColoredString... coloredStrings) {
        for (ColoredString coloredString : coloredStrings) {
            spannableStringBuilder.append(coloredString.string);
            if (coloredString.colorResource != ColoredString.DEFAULT_COLORING) {
                spannableStringBuilder.setSpan(new ForegroundColorSpan(ColorUtils.getColor(context, coloredString
                                .colorResource)), spannableStringBuilder.length() - coloredString.string.length(),
                        spannableStringBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        CharSequence spannableString = spannableStringBuilder.subSequence(0, spannableStringBuilder.length());
        spannableStringBuilder.clear();
        spannableStringBuilder.clearSpans();
        return spannableString;
    }

}
