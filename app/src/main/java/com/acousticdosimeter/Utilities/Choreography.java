package com.acousticdosimeter.Utilities;

/**
 * Class used for holding values used for animations.
 * Created by maciek on 31.01.16.
 */
public class Choreography {
    /**
     * Specifies how long play/pause button should be non clickable after being clicked to avoid too many operations.
     * on recycler view at once
     */
    public static final int PLAY_PAUSE_BUTTON_NOT_ENABLED_INTERVAL = 500;
    /**
     * Specifies time from the start of and app play/pause button should start entering the screen.
     */
    public static final int PLAY_PAUSE_BUTTON_INITIAL_ENTRANCE_TIME = 1000;
    /**
     * Specifies time that play/pause button animation should take.
     */
    public static final int PLAY_PAUSE_BUTTON_ENTRANCE_ANIMATION_TIME = 500;
    /**
     * Specifies how many seconds of spl measurements graph should show.
     */
    public static final int LAST_SECONDS_TO_SHOW_ON_GRAPH = 20;
    /**
     * Specifies time that splash screen fade out animation should take.
     */
    public static final int SPLASH_SCREEN_FADE_OUT_DURATION = 1000;
}
