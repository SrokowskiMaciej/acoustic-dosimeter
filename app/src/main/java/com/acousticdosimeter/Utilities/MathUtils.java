package com.acousticdosimeter.Utilities;

import android.support.annotation.NonNull;

import com.acousticdosimeter.soundprocessing.AudioSettings;

import java.util.List;

/**
 * Utility class used for mathematical calculations
 * Created by maciek on 21.01.16.
 */
public class MathUtils {

    /**
     * Calculates lex 8h value given list of spl readings.
     *
     * @param splsSinceMeasurementStart list of spl measurements
     * @return lex 8h
     */
    public static double calculateLex8hValue(@NonNull List<Double> splsSinceMeasurementStart) {
        double lex8h = 0;
        //Calculating Lex 8h level as specified in polish norm
        for (double leq : splsSinceMeasurementStart) {
            lex8h += leq;
        }
        lex8h = lex8h / ((double) splsSinceMeasurementStart.size());
        //Adding ten logarithms with base ten of fraction of elapsed time to 8 hours
        lex8h += 10 * Math.log10(((AudioSettings.LEQ_READ_PERIOD_SEC * splsSinceMeasurementStart.size())) / 28800.0);
        return lex8h;
    }

    /**
     * Calculates lex w value given lex 8h value.
     *
     * @param lex8h lex 8h
     * @return lex w
     */
    public static double calculateLexWValue(double lex8h) {
        return 10 * Math.log10((Math.pow(10.0, 0.1 * lex8h) / 5.0f));
    }

    /**
     * Rounds floating point number to the closest multiple of a given integer above given floating point number
     *
     * @param from floating point number that we will round up to
     * @param to   integer to the multiple of we will be rounding
     * @return rounded up integer
     */
    public static int roundUpToNearest(float from, int to) {
        return (int) (((from - 1) / to) + 1) * to;
    }

    /**
     * Rounds floating point number to the closest multiple of a given integer below given floating point number
     *
     * @param from floating point number that we will round down to
     * @param to   integer to the multiple of we will be rounding
     * @return rounded down integer
     */
    public static int roundDownToNearest(float from, int to) {
        return (int) (from / to) * to;
    }
}
