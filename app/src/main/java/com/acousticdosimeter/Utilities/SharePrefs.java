package com.acousticdosimeter.Utilities;

/**
 * Class used for holding shared prefs constants
 * Created by Maciek on 2014-09-11.
 */
public class SharePrefs {

    public static final String SHARED_PREF_CALIBRATION_ARG = "sharedPrefCalibrationArg";
    public static final String SHARED_PREF_PATH = "sharedPrefFile";

}
