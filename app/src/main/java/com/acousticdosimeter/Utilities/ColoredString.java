package com.acousticdosimeter.Utilities;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

/**
 * Abstraction used to create spanned strings where every part of the text has color assigned to it.
 * Created by maciek on 29.01.16.
 */
public class ColoredString {
    public String string;
    public int colorResource;

    public static final int DEFAULT_COLORING = -1;

    public ColoredString(String string) {
        this(string, DEFAULT_COLORING);
    }

    public ColoredString(@NonNull String string, @ColorRes int colorResource) {
        this.string = string;
        this.colorResource = colorResource;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

}
