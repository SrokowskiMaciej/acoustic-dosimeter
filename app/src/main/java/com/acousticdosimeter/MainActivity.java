package com.acousticdosimeter;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.acousticdosimeter.UI.CardViewRecyclerAdapter;
import com.acousticdosimeter.UI.GraphView;
import com.acousticdosimeter.UI.PlayPauseFloatingButton;
import com.acousticdosimeter.UI.cards.CalibrationCard;
import com.acousticdosimeter.UI.cards.Card;
import com.acousticdosimeter.UI.cards.DynamicGraphCard;
import com.acousticdosimeter.UI.cards.MeterCard;
import com.acousticdosimeter.UI.cards.PlaceholderCard;
import com.acousticdosimeter.UI.cards.TextCard;
import com.acousticdosimeter.Utilities.Choreography;
import com.acousticdosimeter.Utilities.ColorUtils;
import com.acousticdosimeter.Utilities.ColoredString;
import com.acousticdosimeter.Utilities.MathUtils;
import com.acousticdosimeter.Utilities.SharePrefs;
import com.acousticdosimeter.Utilities.StringUtils;
import com.acousticdosimeter.services.RecordService;
import com.acousticdosimeter.soundprocessing.AudioSettings;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Main activity of an app
 */
public class MainActivity extends SPLDisplayActivity implements PlayPauseFloatingButton
        .OnControlStatusChangeListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    /**
     * States if user started measurement.
     */
    private boolean measuring = false;

    /**
     * Determines if calibration is undergoing.
     */
    private boolean calibrating = false;

    /**
     * Determines if audio permission was granted for app. The app cannot function without it.
     */
    private boolean audioPermissionGranted = false;

    /**
     * Play/Pause button that is placed on the border between top sheet.
     */
    private PlayPauseFloatingButton playPauseButton;

    /**
     * Adapter for {@link Card}s.
     */
    private CardViewRecyclerAdapter cardViewRecyclerAdapter;

    /**
     * List of cards currently displayed in this Activities {@link RecyclerView}
     */
    private List<Card> cardList = new ArrayList<>();

    /**
     * Card showing current level of lex8h and its description if {@link MainActivity#measuring}
     */
    private TextCard lex8hCard;

    /**
     * Card showing current level of lexw and its description if {@link MainActivity#measuring}
     */
    private TextCard lexWCard;

    /**
     * Card showing current SPL reading in both text and linear form.
     */
    private MeterCard meterCard;

    /**
     * Card showing bottom explanatory text about purpose of this app.
     */
    private TextCard mainInfoCard;

    /**
     * Card where user can calibrate readings from the device.
     */
    private CalibrationCard calibrationCard;

    /**
     * Card showing graph of SPL measurements taken in last seconds shown on graph.
     */
    private DynamicGraphCard slpGraphCard;

    /**
     * Number of measurements of 1 second leq values collected from the beginning of the measurement.
     */
    private List<Double> leq1sSinceMeasurementStart = new LinkedList<>();

    /**
     * Number of SPL readings collected from beginning of the measurement.
     */
    private List<Double> splsSinceMeasurementStart = new LinkedList<>();

    /**
     * Overall number of spls delivered. Used to display only some amount of changes in SPL reading.
     */
    private long splsDeliveredCounter;

    /**
     * Unix epoch timestamp regarding when measurement was started.
     */
    private long measurementStartTimestamp;

    /**
     * Number of SPL readings delivered from beginning of measurements. Differs from length of {@link
     * MainActivity#splsSinceMeasurementStart} list as elements from the list have to be removed and not collected
     * indefinitely
     */
    private int splSamplesSinceMeasurementBeginning = 0;

    /**
     * Limit of SPL records hold by {@link MainActivity#splsSinceMeasurementStart} list.
     */
    private static final int SPL_RECORDS_LIMIT = Choreography.LAST_SECONDS_TO_SHOW_ON_GRAPH * AudioSettings
            .SPL_SAMPLING_FREQUENCY;

    /**
     * Holder for strings used by this activity and their colors. Used as optimization instead of always getting
     * strings from resources. Normally this doesn't matter that much but here we are reusing them very often.
     */
    private ReusableStringsHolder reusableStringsHolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupReusableStrings();
        setupCards();
        setupRecyclerView();
        setupPlayPauseButton();
        startEntranceChoreography();
    }


    private void setupCards() {
        String lex8hTitleText = getString(R.string.lex8h_title_text);
        String lex8hInfoText = getString(R.string.lex8h_info_text);
        lex8hCard = new TextCard();
        lex8hCard.setTitleText(lex8hTitleText);
        lex8hCard.setInfoText(lex8hInfoText);

        String lexWTitleText = getString(R.string.lexw_title_text);
        String lexWInfoText = getString(R.string.lexw_info_text);
        lexWCard = new TextCard();
        lexWCard.setTitleText(lexWTitleText);
        lexWCard.setInfoText(lexWInfoText);

        meterCard = new MeterCard();
        meterCard.setTitleText(getString(R.string.meter_title_text));
        meterCard.setInfoText(lex8hInfoText);
        meterCard.setButtonText(getString(R.string.calibration_action_text));
        meterCard.setButtonOnClickListener(this);

        calibrationCard = new CalibrationCard();
        calibrationCard.setProgressListener(this);
        SharedPreferences prefs = getSharedPreferences(SharePrefs.SHARED_PREF_PATH, MODE_PRIVATE);
        int calibration = prefs.getInt(SharePrefs.SHARED_PREF_CALIBRATION_ARG, 0);
        String calibrationString = String.valueOf(calibration);
        reusableStringsHolder.calibrationValueString.setString(calibration > 0 ? "+" + calibrationString :
                calibrationString);
        calibrationCard.setInitialProgress(calibration + 25);
        calibrationCard.setTitleText(StringUtils.getSpannableString(this, reusableStringsHolder.calibrationIntroString,
                reusableStringsHolder.calibrationValueString, reusableStringsHolder.calibrationUnitString));
        calibrationCard.setInfoText(getString(R.string.calibration_info_text));

        GraphView.GraphDimensions graphDimensions = new GraphView.GraphDimensions(105, 7, SPL_RECORDS_LIMIT, 20);
        slpGraphCard = new DynamicGraphCard(splsSinceMeasurementStart, graphDimensions);
        slpGraphCard.setTitleText(getString(R.string.leq_title_text));
        slpGraphCard.setInfoText(getString(R.string.leq_info_text));
        slpGraphCard.setDescriptionText(getString(R.string.leq_description_text));

        mainInfoCard = new TextCard();
        mainInfoCard.setInfoText(getString(R.string.purpose_explanation_text));
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        cardViewRecyclerAdapter = new CardViewRecyclerAdapter(cardList);
        recyclerView.setAdapter(cardViewRecyclerAdapter);
    }

    private void setupPlayPauseButton() {
        playPauseButton = (PlayPauseFloatingButton) findViewById(R.id.playPauseButton);
        playPauseButton.setColor(ColorUtils.getColor(this, android.R.color.white));
        playPauseButton.setCircularBackgroundColor(ColorUtils.getColor(this, R.color.accent));
        playPauseButton.setOnControlStatusChangeListener(this);
    }

    private void showInitialCards() {
        //Add initial cards to show
        PlaceholderCard placeholderCard = new PlaceholderCard();
        cardList.add(placeholderCard);
        cardList.add(meterCard);
        cardList.add(mainInfoCard);
        cardViewRecyclerAdapter.notifyItemRangeInserted(cardList.indexOf(placeholderCard), 3);
    }


    private void setupReusableStrings() {
        reusableStringsHolder = new ReusableStringsHolder();
        reusableStringsHolder.unitDbString = getString(R.string.db_unit);

        reusableStringsHolder.timespanString = new ColoredString(getString(R.string.timespan_text));
        reusableStringsHolder.formattedTimeString = new ColoredString("", R.color.accent);

        reusableStringsHolder.meterIntroText = new ColoredString(getString(R.string.meter_title_text));
        reusableStringsHolder.meterSplText = new ColoredString("", R.color.accent);
        reusableStringsHolder.meterUnitText = new ColoredString(getString(R.string.meter_unit_text));


        reusableStringsHolder.lex8IntroString = new ColoredString(getString(R.string.lex8h_measurement_text));
        reusableStringsHolder.lex8hString = new ColoredString("", R.color.accent);

        reusableStringsHolder.lexWIntroString = new ColoredString(getString(R.string.lexw_measurement_text),
                ColoredString.DEFAULT_COLORING);
        reusableStringsHolder.lexWString = new ColoredString("", R.color.accent);

        reusableStringsHolder.calibrationIntroString = new ColoredString(getString(R.string
                .calibration_current_text));
        reusableStringsHolder.calibrationValueString = new ColoredString("", R.color.accent);
        reusableStringsHolder.calibrationUnitString = new ColoredString(reusableStringsHolder.unitDbString);
    }

    private void startEntranceChoreography() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showStartMeasurementButton(new PlayPauseButtonAnimationListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        checkForGrantedPermission();
                    }
                });
            }
        }, Choreography.PLAY_PAUSE_BUTTON_INITIAL_ENTRANCE_TIME);
    }

    private void hideSplashScreen() {
        findViewById(R.id.splash_screen).animate().alpha(0).setDuration(Choreography.SPLASH_SCREEN_FADE_OUT_DURATION)
                .setListener(new AnimatorListenerAdapter
                        () {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        showInitialCards();
                    }
                });
    }

    private void checkForGrantedPermission() {
        if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager
                .PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        } else {
            audioPermissionGranted = true;
            hideSplashScreen();
            startRecordService();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            hideSplashScreen();
            audioPermissionGranted = true;
            startRecordService();
        } else {
            Toast.makeText(this, "This app won't work without Microphone permission", Toast.LENGTH_LONG).show();
            finish();
        }

    }

    @Override
    public void onStatusChange(View view, final boolean state) {
        if (state) {
            showMeasurementViews();
            measurementStartTimestamp = System.currentTimeMillis();
        } else {
            measuring = false;
            splSamplesSinceMeasurementBeginning = 0;
            hideMeasurementViews();
            resetRecordValues();
        }
        playPauseButton.setClickable(false);
        //Used to disable button until animations on recycler view end
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (state) {
                    measuring = true;
                    splSamplesSinceMeasurementBeginning = 0;
                }
                playPauseButton.setClickable(true);
            }
        }, Choreography.PLAY_PAUSE_BUTTON_NOT_ENABLED_INTERVAL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startRecordService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // If measurement is taking place when activity stops to be foreground
        // stop measurement and hide associated views
        if (measuring) {
            measuring = false;
            splSamplesSinceMeasurementBeginning = 0;
            cardList.remove(lex8hCard);
            cardList.remove(lexWCard);
            cardList.remove(slpGraphCard);
            playPauseButton.setPlayed(false);
            resetRecordValues();
            cardViewRecyclerAdapter.notifyDataSetChanged();

        }
        stopRecordService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Method called with every new Spl ready to e showed
     *
     * @param spl Read SPL
     */
    protected void SPLReady(final double spl) {
        splsDeliveredCounter++;
        if (measuring) {
            splsSinceMeasurementStart.add(spl);
            if (splsSinceMeasurementStart.size() > SPL_RECORDS_LIMIT) {
                splsSinceMeasurementStart.remove(0);
            }
            splSamplesSinceMeasurementBeginning++;
            slpGraphCard.setSamplesShown(splSamplesSinceMeasurementBeginning);
            cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(slpGraphCard));

        }
        if (splsDeliveredCounter % 2 == 0) {
            meterCard.setReading(spl);
            reusableStringsHolder.meterSplText.setString(String.format("%.1f", spl));
            meterCard.setTitleText(StringUtils.getSpannableString(this, reusableStringsHolder.meterIntroText,
                    reusableStringsHolder.meterSplText, reusableStringsHolder.meterUnitText));
            cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(meterCard));
        }
    }

    @Override
    protected void LEQ1sReady(double LEQ) {
        if (measuring) {
            leq1sSinceMeasurementStart.add(LEQ);

            reusableStringsHolder.formattedTimeString.setString(StringUtils.getFormattedTime(
                    System.currentTimeMillis() - measurementStartTimestamp));

            double lex8hValue = MathUtils.calculateLex8hValue(leq1sSinceMeasurementStart);
            reusableStringsHolder.lex8hString.setString(String.format("%.1f", lex8hValue) +
                    reusableStringsHolder
                            .unitDbString);
            CharSequence lex8hColoredString = StringUtils.getSpannableString(this,
                    reusableStringsHolder.lex8IntroString, reusableStringsHolder.lex8hString,
                    reusableStringsHolder
                            .timespanString, reusableStringsHolder.formattedTimeString);
            lex8hCard.setDescriptionText(lex8hColoredString);

            double lexWValue = MathUtils.calculateLexWValue(lex8hValue);
            reusableStringsHolder.lexWString.setString(String.format("%.1f", lexWValue) + reusableStringsHolder
                    .unitDbString);
            CharSequence lexWColoredString = StringUtils.getSpannableString(this,
                    reusableStringsHolder.lexWIntroString,
                    reusableStringsHolder.lexWString, reusableStringsHolder.timespanString,
                    reusableStringsHolder
                            .formattedTimeString);
            lexWCard.setDescriptionText(lexWColoredString);

            cardViewRecyclerAdapter.notifyItemRangeChanged(cardList.indexOf(lex8hCard), 2);
        }
    }

    private void resetRecordValues() {
        leq1sSinceMeasurementStart.clear();
        splsSinceMeasurementStart.clear();
    }

    private void startRecordService() {
        if (audioPermissionGranted) {
            Intent intent = new Intent(getApplicationContext(), RecordService.class);
            startService(intent);
        }
    }

    private void stopRecordService() {
        Intent intent = new Intent(getApplicationContext(), RecordService.class);
        stopService(intent);

    }

    private void showMeasurementViews() {
        int cardPositionAfterMeterCard = cardList.indexOf(meterCard) + 1;
        cardList.add(cardPositionAfterMeterCard, lexWCard);
        cardList.add(cardPositionAfterMeterCard, lex8hCard);
        cardList.add(cardPositionAfterMeterCard, slpGraphCard);
        cardViewRecyclerAdapter.notifyItemRangeInserted(cardPositionAfterMeterCard, 3);
    }

    private void hideMeasurementViews() {
        int slpGraphCardCardPosition = cardList.indexOf(slpGraphCard);
        cardList.remove(lexWCard);
        cardList.remove(lex8hCard);
        cardList.remove(slpGraphCard);
        cardViewRecyclerAdapter.notifyItemRangeRemoved(slpGraphCardCardPosition, 3);
    }

    private void startCalibration() {
        if (measuring) {
            hideMeasurementViews();
        }
        int mainInfoCardPosition = cardList.indexOf(mainInfoCard);
        cardList.remove(mainInfoCard);
        cardViewRecyclerAdapter.notifyItemRemoved(mainInfoCardPosition);
        hideStartMeasurementButton(new PlayPauseButtonAnimationListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                meterCard.setButtonOnClickListener(MainActivity.this);
                cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(meterCard));
                cardList.add(calibrationCard);
                cardViewRecyclerAdapter.notifyItemInserted(cardList.indexOf(calibrationCard));
            }
        });
        calibrating = true;
        meterCard.setButtonOnClickListener(null);
        meterCard.setButtonText(getString(R.string.calibration_confirmation_button_text));
        cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(meterCard));
    }

    private void stopCalibration() {
        int calibrationCardPosition = cardList.indexOf(calibrationCard);
        cardList.remove(calibrationCard);
        cardViewRecyclerAdapter.notifyItemRemoved(calibrationCardPosition);
        showStartMeasurementButton(new PlayPauseButtonAnimationListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                meterCard.setButtonOnClickListener(MainActivity.this);
                cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(meterCard));
                if (measuring) {
                    showMeasurementViews();
                }
                cardList.add(mainInfoCard);
                int mainInfoCardPosition = cardList.indexOf(mainInfoCard);
                cardViewRecyclerAdapter.notifyItemInserted(mainInfoCardPosition);

            }
        });
        calibrating = false;
        meterCard.setButtonOnClickListener(null);
        meterCard.setButtonText(getString(R.string.calibration_action_text));
        cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(meterCard));

    }

    private void showStartMeasurementButton(@NonNull Animator.AnimatorListener animatorListener) {
        playPauseButton.animate().x(getResources().getDisplayMetrics().widthPixels / 6).setDuration
                (Choreography.PLAY_PAUSE_BUTTON_ENTRANCE_ANIMATION_TIME).setInterpolator(new
                FastOutSlowInInterpolator()).setListener(animatorListener).start();
    }

    private void hideStartMeasurementButton(@NonNull Animator.AnimatorListener animatorListener) {
        playPauseButton.setClickable(false);
        playPauseButton.animate().translationX(-playPauseButton.getWidth()).setDuration
                (Choreography.PLAY_PAUSE_BUTTON_ENTRANCE_ANIMATION_TIME).setInterpolator(new
                FastOutSlowInInterpolator()).setListener(animatorListener).start();
    }

    @Override
    public void onClick(View v) {
        if (calibrating) {
            stopCalibration();
        } else {
            startCalibration();
        }

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //Used from calibration card
        if (fromUser) {
            // Max progress for this card is 50 so we are substituting 25 from it in order for 0 to be in the middle
            int calibration = progress - 25;
            Intent intent = new Intent(RecordService.ACTION_CALIBRATION);
            intent.putExtra(RecordService.CALIBRATION_ARG, calibration);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            SharedPreferences.Editor editor = getSharedPreferences(SharePrefs.SHARED_PREF_PATH, MODE_PRIVATE).edit();
            editor.putInt(SharePrefs.SHARED_PREF_CALIBRATION_ARG, calibration);
            editor.apply();
            String calibrationString = String.valueOf(calibration);
            reusableStringsHolder.calibrationValueString.setString(calibration > 0 ? "+" + calibrationString :
                    calibrationString);
            calibrationCard.setTitleText(StringUtils.getSpannableString(this, reusableStringsHolder
                            .calibrationIntroString,

                    reusableStringsHolder.calibrationValueString, reusableStringsHolder.calibrationUnitString));
            cardViewRecyclerAdapter.notifyItemChanged(cardList.indexOf(calibrationCard));
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private class PlayPauseButtonAnimationListener extends AnimatorListenerAdapter {
        @Override
        public void onAnimationEnd(Animator animation) {
            playPauseButton.setClickable(true);
        }

        @Override
        public void onAnimationStart(Animator animation) {
            playPauseButton.setClickable(false);
        }
    }

    /**
     * Class used for holding strings as well as their color if such a need arises.
     */
    private class ReusableStringsHolder {
        String unitDbString;

        ColoredString timespanString;
        ColoredString formattedTimeString;

        ColoredString meterIntroText;
        ColoredString meterSplText;
        ColoredString meterUnitText;

        ColoredString lex8IntroString;
        ColoredString lex8hString;

        ColoredString lexWIntroString;
        ColoredString lexWString;

        ColoredString calibrationIntroString;
        ColoredString calibrationValueString;
        ColoredString calibrationUnitString;
    }
}
