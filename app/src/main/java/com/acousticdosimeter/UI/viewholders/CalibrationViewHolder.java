package com.acousticdosimeter.UI.viewholders;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.acousticdosimeter.R;
import com.acousticdosimeter.UI.cards.CalibrationCard;
import com.acousticdosimeter.UI.cards.Card;

/**
 * Created by maciek on 31.01.16.
 */
public class CalibrationViewHolder extends CardViewHolder {

    private TextView titleTextView, infoTextView;
    private SeekBar calibrationSeekBar;
    private boolean initialCalibrationSet;

    public CalibrationViewHolder(ViewGroup parent) {
        super(parent, R.layout.card_calibrator);
        titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
        infoTextView = (TextView) itemView.findViewById(R.id.infoTextView);
        calibrationSeekBar = (SeekBar) itemView.findViewById(R.id.calibrationSeekBar);
    }

    @Override
    public void populateViews(Card card) {
        if (card instanceof CalibrationCard) {
            CalibrationCard calibrationCard = (CalibrationCard) card;
            if (TextUtils.isEmpty(calibrationCard.getTitleText())) {
                titleTextView.setVisibility(View.GONE);
            } else {
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(calibrationCard.getTitleText());
            }
            if (TextUtils.isEmpty(calibrationCard.getInfoText())) {
                infoTextView.setVisibility(View.GONE);
            } else {
                infoTextView.setVisibility(View.VISIBLE);
                infoTextView.setText(calibrationCard.getInfoText());
            }
            if(!initialCalibrationSet){
                calibrationSeekBar.setProgress(calibrationCard.getInitialProgress());
                initialCalibrationSet=true;
            }
            if (calibrationCard.getProgressListener() != null) {
                calibrationSeekBar.setOnSeekBarChangeListener(calibrationCard.getProgressListener());
            }
        } else {
            throw new IllegalArgumentException("MeterViewHolder can only populate views " +
                    "with data from MeterCard object");
        }
    }
}
