package com.acousticdosimeter.UI.viewholders;

import android.animation.ObjectAnimator;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.acousticdosimeter.R;
import com.acousticdosimeter.UI.cards.Card;
import com.acousticdosimeter.UI.cards.MeterCard;
import com.acousticdosimeter.soundprocessing.AudioSettings;

/**
 * View holder for {@link MeterCard} type of card. Contains basic three types of {@link TextView}, {@link Button} and
 * {@link ProgressBar}
 * Created by maciek on 07.01.16.
 */
public class MeterViewHolder extends CardViewHolder {

    private TextView titleTextView, descriptionTextView, infoTextView;
    private Button button;
    private ProgressBar readingProgressBar;

    public MeterViewHolder(ViewGroup parent) {
        super(parent, R.layout.card_meter);
        titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
        descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
        infoTextView = (TextView) itemView.findViewById(R.id.infoTextView);
        readingProgressBar = (ProgressBar) itemView.findViewById(R.id.readingProgressBar);
        button = (Button) itemView.findViewById(R.id.button);
    }

    @Override
    public void populateViews(Card card) {
        if (card instanceof MeterCard) {
            MeterCard meterCard = (MeterCard) card;
            if (TextUtils.isEmpty(meterCard.getTitleText())) {
                titleTextView.setVisibility(View.GONE);
            } else {
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(meterCard.getTitleText());
            }
            if (TextUtils.isEmpty(meterCard.getDescriptionText())) {
                descriptionTextView.setVisibility(View.GONE);
            } else {
                descriptionTextView.setVisibility(View.VISIBLE);
                descriptionTextView.setText(meterCard.getDescriptionText());
            }
            if (TextUtils.isEmpty(meterCard.getInfoText())) {
                infoTextView.setVisibility(View.GONE);
            } else {
                infoTextView.setVisibility(View.VISIBLE);
                infoTextView.setText(meterCard.getInfoText());
            }
            if (TextUtils.isEmpty(meterCard.getButtonText())) {
                button.setVisibility(View.GONE);
            } else {
                button.setVisibility(View.VISIBLE);
                button.setText(meterCard.getButtonText());
            }
            button.setOnClickListener(meterCard.getButtonOnClickListener());
            ObjectAnimator animation = ObjectAnimator.ofInt(readingProgressBar, "progress", (int) meterCard
                    .getReading());
            animation.setDuration(1000 / AudioSettings.SPL_SAMPLING_FREQUENCY - 20);
            animation.setInterpolator(new AccelerateInterpolator());
            animation.start();
        } else {
            throw new IllegalArgumentException("MeterViewHolder can only populate views " +
                    "with data from MeterCard object");
        }
    }
}
