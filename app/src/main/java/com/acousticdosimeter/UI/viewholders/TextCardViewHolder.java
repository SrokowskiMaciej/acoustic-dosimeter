package com.acousticdosimeter.UI.viewholders;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acousticdosimeter.R;
import com.acousticdosimeter.UI.cards.Card;
import com.acousticdosimeter.UI.cards.TextCard;

/**
 * View holder for {@link TextCard} type of card.
 * Created by maciek on 06.01.16.
 */
public class TextCardViewHolder extends CardViewHolder {

    private TextView titleTextView, descriptionTextView, infoTextView;

    public TextCardViewHolder(ViewGroup parent) {
        super(parent, R.layout.card_text);
        titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
        descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
        infoTextView = (TextView) itemView.findViewById(R.id.infoTextView);
    }

    @Override
    public void populateViews(Card card) {
        if (card instanceof TextCard) {
            TextCard textCard = (TextCard) card;
            if (TextUtils.isEmpty(textCard.getTitleText())) {
                titleTextView.setVisibility(View.GONE);
            } else {
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(textCard.getTitleText());
            }
            if (TextUtils.isEmpty(textCard.getDescriptionText())) {
                descriptionTextView.setVisibility(View.GONE);
            } else {
                descriptionTextView.setVisibility(View.VISIBLE);
                descriptionTextView.setText(textCard.getDescriptionText());
            }
            if (TextUtils.isEmpty(textCard.getInfoText())) {
                infoTextView.setVisibility(View.GONE);
            } else {
                infoTextView.setVisibility(View.VISIBLE);
                infoTextView.setText(textCard.getInfoText());
            }
        } else {
            throw new IllegalArgumentException("TextCardViewHolder can only populate views " +
                    "with data from TextCard object");
        }
    }
}