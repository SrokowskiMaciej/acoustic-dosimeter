package com.acousticdosimeter.UI.viewholders;

import android.view.ViewGroup;

import com.acousticdosimeter.R;
import com.acousticdosimeter.UI.cards.Card;

/**
 * Dummy view holder used to make space on recycler view.
 * Created by maciek on 07.01.16.
 */
public class PlaceholderViewHolder extends CardViewHolder {

    public PlaceholderViewHolder(ViewGroup parent) {
        super(parent, R.layout.card_placeholder);
    }

    @Override
    public void populateViews(Card card) {
        //Nothing as is is only used to take space of recycler view.
    }
}
