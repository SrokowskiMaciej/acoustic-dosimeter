package com.acousticdosimeter.UI.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.acousticdosimeter.UI.cards.Card;

/**
 * Created by maciek on 06.01.16.
 */

public abstract class CardViewHolder extends RecyclerView.ViewHolder {

    public CardViewHolder(ViewGroup parent, int layoutResource) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false));
    }

    public abstract void populateViews(Card card);
}