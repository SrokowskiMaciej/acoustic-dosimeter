package com.acousticdosimeter.UI.viewholders;

import android.view.ViewGroup;
import android.widget.TextView;

import com.acousticdosimeter.R;
import com.acousticdosimeter.UI.GraphView;
import com.acousticdosimeter.UI.cards.Card;
import com.acousticdosimeter.UI.cards.DynamicGraphCard;

/**
 * Created by maciek on 07.01.16.
 */
public class DynamicGraphViewHolder extends CardViewHolder {

    private TextView titleTextView, descriptionTextView, infoTextView;
    private GraphView graphView;

    public DynamicGraphViewHolder(ViewGroup parent) {
        super(parent, R.layout.card_graph);
        titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
        descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
        infoTextView = (TextView) itemView.findViewById(R.id.infoTextView);
        graphView = (GraphView) itemView.findViewById(R.id.graphView);
    }

    @Override
    public void populateViews(Card card) {
        if (card instanceof DynamicGraphCard) {
            DynamicGraphCard graphCard = (DynamicGraphCard) card;
            titleTextView.setText(graphCard.getTitleText());
            descriptionTextView.setText(graphCard.getDescriptionText());
            infoTextView.setText(graphCard.getInfoText());
            graphView.setSamples(graphCard.getCurrentSamples());
            graphView.setSamplesShown(graphCard.getSamplesShown());
            graphView.setGraphDimensions(graphCard.getGraphDimensions());
        } else {
            throw new IllegalArgumentException("GraphViewHolder can only populate views " +
                    "with data from GraphCard object");
        }
    }
}
