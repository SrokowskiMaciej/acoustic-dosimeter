package com.acousticdosimeter.UI;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.acousticdosimeter.UI.cards.Card;
import com.acousticdosimeter.UI.viewholders.CalibrationViewHolder;
import com.acousticdosimeter.UI.viewholders.CardViewHolder;
import com.acousticdosimeter.UI.viewholders.DynamicGraphViewHolder;
import com.acousticdosimeter.UI.viewholders.MeterViewHolder;
import com.acousticdosimeter.UI.viewholders.PlaceholderViewHolder;
import com.acousticdosimeter.UI.viewholders.TextCardViewHolder;

import java.util.List;

/**
 * Recycler View adapter suited for showing data from {@link Card}s
 * Created by maciek on 04.01.16.
 */
public class CardViewRecyclerAdapter extends RecyclerView.Adapter<CardViewHolder> {

    private List<Card> cards;

    /**
     * Constructor setting {@link List} of {@link Card}s to use by this adapter.
     *
     * @param cards {@link List} of {@link Card}s to show.
     */
    public CardViewRecyclerAdapter(List<Card> cards) {
        this.cards = cards;
    }

    /**
     * Returns {@link List} of {@link Card}s given to it in constructor
     *
     * @return List of {@link Card}s used by this adapter.l
     */
    public List<Card> getCards() {
        return cards;
    }

    /**
     * Sets {@link List} of {@link Card}s to use for this adapter
     *
     * @param cards {@link List} of {@link Card}s to use
     */
    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardViewHolder viewHolder = null;
        if (Card.Type.PLACEHOLDER.getNumber() == viewType) {
            viewHolder = new PlaceholderViewHolder(parent);
        } else if (Card.Type.TEXT.getNumber() == viewType) {
            viewHolder = new TextCardViewHolder(parent);
        } else if (Card.Type.METER.getNumber() == viewType) {
            viewHolder = new MeterViewHolder(parent);
        } else if (Card.Type.CALIBRATION.getNumber() == viewType) {
            viewHolder = new CalibrationViewHolder(parent);
        } else if (Card.Type.GRAPH.getNumber() == viewType) {
            viewHolder = new DynamicGraphViewHolder(parent);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        holder.populateViews(cards.get(position));
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    @Override
    public int getItemViewType(int position) {
        return cards.get(position).getType().getNumber();
    }
}
