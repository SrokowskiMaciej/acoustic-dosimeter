package com.acousticdosimeter.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.acousticdosimeter.R;
import com.acousticdosimeter.Utilities.MathUtils;

import java.util.ArrayList;
import java.util.List;

import static com.acousticdosimeter.Utilities.ColorUtils.getColor;

/**
 * View used for displaying array of SPL readings
 */
public class GraphView extends View {

    /**
     * Array of SPL readings to display
     */
    private List<Double> mSamples = new ArrayList<>(0);

    private int samplesShown;

    /**
     * Graph dimensions with default values
     */
    private GraphDimensions graphDimensions = new GraphDimensions(100, 5, 100, 5);

    /**
     * Paints used for drawing oon canvas
     */
    private final static Paint mPointsPaint;
    private final static Paint mHorizontalLinesPaint;
    private final static Paint mVerticalLinesPaint;
    private final static Paint mTextPaint;
    private final static Paint mBoundsPaint;
    private final static Paint mBlankPaint;
    /**
     * Margins in pixels used for specifying graph size
     */
    private final static float TEXT_HEIGHT = 25;
    private final static float TOP_MARGIN = 20;
    private final static float LEFT_MARGIN = 65;
    private final static float RIGHT_MARGIN = 20;
    private final static float BOTTOM_MARGIN = 35;

    public GraphView(Context context) {
        super(context);
        setupColors();
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupColors();
    }

    public GraphView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setupColors();
    }

    static {
        mPointsPaint = new Paint();
        mPointsPaint.setStyle(Paint.Style.STROKE);
        mPointsPaint.setStrokeWidth(5.0f);
        mHorizontalLinesPaint = new Paint();
        mHorizontalLinesPaint.setStyle(Paint.Style.STROKE);
        mHorizontalLinesPaint.setStrokeWidth(3.0f);
        mHorizontalLinesPaint.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
        mVerticalLinesPaint = new Paint();
        mVerticalLinesPaint.setStyle(Paint.Style.STROKE);
        mVerticalLinesPaint.setStrokeWidth(3.0f);
        mTextPaint = new Paint();
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(TEXT_HEIGHT);
        mBoundsPaint = new Paint();
        mBoundsPaint.setStyle(Paint.Style.STROKE);
        mBoundsPaint.setStrokeWidth(5.0f);
        mBlankPaint = new Paint();
        mBlankPaint.setStyle(Paint.Style.FILL);
    }

    /**
     * Method used for setting up colors for paints
     */
    private void setupColors() {
        mPointsPaint.setColor(getColor(getContext(), R.color.accent));
        mHorizontalLinesPaint.setColor(getColor(getContext(), R.color.divider));
        mVerticalLinesPaint.setColor(getColor(getContext(), R.color.divider));
        mTextPaint.setColor(getColor(getContext(), R.color.primary_text));
        mBoundsPaint.setColor(getColor(getContext(), R.color.divider));
        mBlankPaint.setColor(getColor(getContext(), R.color.cardview_light_background));
    }

    /**
     * Method used for setting array of SPL readings to be drawn
     *
     * @param samples Array of SPL readings
     */
    public synchronized void setSamples(List<Double> samples) {
        this.mSamples = samples;
        invalidate();
    }

    /**
     * Sets number of samples that should be currently shown on a graph.
     *
     * @param samplesShown samples to show
     */
    public void setSamplesShown(int samplesShown) {
        this.samplesShown = samplesShown;
    }

    /**
     * Sets graph dimensions.
     *
     * @param graphDimensions graph dimensions.
     */
    public void setGraphDimensions(GraphDimensions graphDimensions) {
        this.graphDimensions = graphDimensions;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //Render blank
        canvas.drawColor(getColor(getContext(), R.color.cardview_light_background));
        //Save dimensions of a canvas so it can be restored later
        canvas.save();
        //Scale and translate canvas so its starting point will be in o,o point of a graph
        float scaleX = 1 - (LEFT_MARGIN + RIGHT_MARGIN) / (float) canvas.getWidth();
        float scaleY = 1 - (TOP_MARGIN + BOTTOM_MARGIN) / (float) canvas.getHeight();
        canvas.scale(scaleX, scaleY, 0, 0);
        float translateX = LEFT_MARGIN;
        float translateY = TOP_MARGIN + canvas.getHeight();
        canvas.translate(translateX, translateY);

        //Calculate number of pixels per one Decibel so it will be easier to draw
        float pixelsPerDecibel = ((float) canvas.getHeight()) / ((float) graphDimensions.valuesRange);
        //Calculate number of pixels per one one SPL reading  so it will be easier to draw
        float pixelsPerSample = ((float) canvas.getWidth()) / ((float) graphDimensions.samplesShownLimit);

        drawVerticalLines(canvas, samplesShown, pixelsPerSample);
//        drawTimeDescriptions(canvas);
        drawHorizontalLines(canvas);
        drawDecibelsDescriptions(canvas);
        drawBounds(canvas);
        drawResults(canvas, mSamples, pixelsPerDecibel, pixelsPerSample);
        canvas.restore();

    }

    private synchronized void drawResults(Canvas canvas, List<Double> samples, float pixelsPerQuant, float
            pixelsPerPeriod) {

        //Draw results
        Path path = new Path();

        for (int sampleNumber = 0; sampleNumber < samples.size(); sampleNumber++) {
            if (samples.get(sampleNumber) > 5) {
                float x = ((float) sampleNumber) * pixelsPerPeriod;
                float y = -samples.get(sampleNumber).floatValue() * pixelsPerQuant;
                path.lineTo(x, y);
            } else {
                float x = ((float) sampleNumber) * pixelsPerPeriod;
                float y = -samples.get(sampleNumber).floatValue() * pixelsPerQuant;
                path.moveTo(x, y);
            }
        }

        canvas.drawPath(path, mPointsPaint);
    }


    private void drawHorizontalLines(Canvas canvas) {
        float[] horizontalGridLinesPoints = new float[graphDimensions.valuesLevelsMarked * 4 + 4];

        for (int i = 0; i <= graphDimensions.valuesLevelsMarked; i++) {
            horizontalGridLinesPoints[4 * i] = 0;
            horizontalGridLinesPoints[4 * i + 1] = -((float) canvas.getHeight()) * (((float) i) / (float)
                    graphDimensions.valuesLevelsMarked);
            horizontalGridLinesPoints[4 * i + 2] = canvas.getWidth();
            horizontalGridLinesPoints[4 * i + 3] = -((float) canvas.getHeight()) * (((float) i) / (float)
                    graphDimensions.valuesLevelsMarked);

        }
        canvas.drawLines(horizontalGridLinesPoints, mHorizontalLinesPaint);
    }

    private void drawDecibelsDescriptions(Canvas canvas) {

        for (int i = 0; i <= graphDimensions.valuesLevelsMarked; i++) {
            float description = ((float) graphDimensions.valuesRange / (float) graphDimensions.valuesLevelsMarked) *
                    ((float) i);
            float yPosition = -canvas.getHeight() * ((float) i) / (float) graphDimensions.valuesLevelsMarked +
                    TEXT_HEIGHT / 2;
            float xPosition = -LEFT_MARGIN + 5;
            canvas.drawText(String.valueOf((int) description), xPosition, yPosition, mTextPaint);
        }
    }

    private void drawVerticalLines(Canvas canvas, int allSamplesShown, float pixelsPerSample) {


        int samplesInCurrentSecond = 0;
        int samplesPerMarkedLevel = graphDimensions.samplesShownLimit / graphDimensions.samplesLevelsMarked;
        //Find number of samples that is in the bounds of current second
        if (allSamplesShown > graphDimensions.samplesShownLimit) {
            int samplesInFullShownSeconds = MathUtils.roundDownToNearest((float) allSamplesShown,
                    samplesPerMarkedLevel);
            samplesInCurrentSecond = allSamplesShown - samplesInFullShownSeconds;
        }
        //Draw lines
        float[] verticalGridLinesPoints = new float[graphDimensions.samplesLevelsMarked * 4 + 4];
        for (int i = 0; i <= graphDimensions.samplesLevelsMarked; i++) {
            verticalGridLinesPoints[4 * i] = (i * samplesPerMarkedLevel - samplesInCurrentSecond) * pixelsPerSample;
            verticalGridLinesPoints[4 * i + 1] = -canvas.getHeight();
            verticalGridLinesPoints[4 * i + 2] = (i * samplesPerMarkedLevel - samplesInCurrentSecond) * pixelsPerSample;
            verticalGridLinesPoints[4 * i + 3] = 0;
        }
        canvas.drawLines(verticalGridLinesPoints, mVerticalLinesPaint);
        canvas.drawRect(-LEFT_MARGIN, -canvas.getHeight() - BOTTOM_MARGIN, -1, 0, mBlankPaint);
    }

    private void drawBounds(Canvas canvas) {
        canvas.drawRect(0, -canvas.getHeight(), canvas.getWidth(), 0, mBoundsPaint);
    }

    /**
     * Class holding dimensions given to the graph.
     */
    public static class GraphDimensions {

        public GraphDimensions(int valuesRange, int valuesLevelsMarked, int samplesShownLimit, int
                samplesLevelsMarked) {
            this.valuesRange = valuesRange;
            this.valuesLevelsMarked = valuesLevelsMarked;
            this.samplesShownLimit = samplesShownLimit;
            this.samplesLevelsMarked = samplesLevelsMarked;
        }

        /**
         * Values range that graph should display. Always starts from 0.
         */
        public final int valuesRange;
        /**
         * Number of horizontal lines shown on values dataset
         */
        public final int valuesLevelsMarked;

        /**
         * Samples range that graph should display. On the left of the graph there would
         * be 0 - samplesShownLimit
         */
        public final int samplesShownLimit;

        /**
         * Number of horizontal lines shown samples dataset
         */
        public final int samplesLevelsMarked;
    }

}
