package com.acousticdosimeter.UI.cards;

/**
 * Created by maciek on 04.01.16.
 */
public abstract class Card {

    private final Type type;

    private CharSequence titleText, descriptionText, infoText = "";

    public Card(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public CharSequence getTitleText() {
        return titleText;
    }

    public void setTitleText(CharSequence titleText) {
        this.titleText = titleText;
    }

    public CharSequence getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(CharSequence descriptionText) {
        this.descriptionText = descriptionText;
    }

    public CharSequence getInfoText() {
        return infoText;
    }

    public void setInfoText(CharSequence infoText) {
        this.infoText = infoText;
    }

    public enum Type {
        PLACEHOLDER(0), TEXT(1), METER(2), GRAPH(3), CALIBRATION(4);
        private final int type;

        private Type(int type) {
            this.type = type;
        }

        public int getNumber() {
            return type;
        }
    }
}
