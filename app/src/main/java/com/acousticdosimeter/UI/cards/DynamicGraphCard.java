package com.acousticdosimeter.UI.cards;

import com.acousticdosimeter.UI.GraphView;

import java.util.List;

/**
 * Created by maciek on 05.01.16.
 */
public class DynamicGraphCard extends Card {

    private final List<Double> currentSamples;
    private final GraphView.GraphDimensions graphDimensions;
    private int samplesShown = 0;

    public DynamicGraphCard(List<Double> currentSamples, GraphView.GraphDimensions graphDimensions) {
        super(Type.GRAPH);
        this.currentSamples = currentSamples;
        this.graphDimensions = graphDimensions;
    }

    public List<Double> getCurrentSamples() {
        return currentSamples;
    }

    public int getSamplesShown() {
        return samplesShown;
    }

    public void setSamplesShown(int samplesShown) {
        this.samplesShown = samplesShown;
    }

    public GraphView.GraphDimensions getGraphDimensions() {
        return graphDimensions;
    }
}
