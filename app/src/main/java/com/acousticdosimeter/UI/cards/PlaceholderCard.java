package com.acousticdosimeter.UI.cards;

/**
 * Created by maciek on 07.01.16.
 */
public class PlaceholderCard extends Card {

    public PlaceholderCard() {
        super(Type.PLACEHOLDER);
    }
}
