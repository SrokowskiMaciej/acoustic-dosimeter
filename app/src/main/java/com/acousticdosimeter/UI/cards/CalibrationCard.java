package com.acousticdosimeter.UI.cards;

import android.widget.SeekBar;

/**
 * Created by maciek on 31.01.16.
 */
public class CalibrationCard extends Card {

    private SeekBar.OnSeekBarChangeListener progressListener;
    private int initialProgress;

    public CalibrationCard() {
        super(Type.CALIBRATION);
    }

    public SeekBar.OnSeekBarChangeListener getProgressListener() {
        return progressListener;
    }

    public void setProgressListener(SeekBar.OnSeekBarChangeListener progressListener) {
        this.progressListener = progressListener;
    }

    public int getInitialProgress() {
        return initialProgress;
    }

    public void setInitialProgress(int initialProgress) {
        this.initialProgress = initialProgress;
    }
}
