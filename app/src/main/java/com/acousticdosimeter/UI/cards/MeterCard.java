package com.acousticdosimeter.UI.cards;

import android.widget.Button;

/**
 * Created by maciek on 05.01.16.
 */
public class MeterCard extends Card {

    private String buttonText = "";
    private Button.OnClickListener buttonOnClickListener;
    private double reading;

    public MeterCard() {
        super(Type.METER);
    }

    public void setReading(double reading) {
        this.reading = reading;
    }

    public double getReading() {
        return reading;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonText() {
        return buttonText;
    }

    public Button.OnClickListener getButtonOnClickListener() {
        return buttonOnClickListener;
    }

    public void setButtonOnClickListener(Button.OnClickListener buttonOnClickListener) {
        this.buttonOnClickListener = buttonOnClickListener;
    }
}
