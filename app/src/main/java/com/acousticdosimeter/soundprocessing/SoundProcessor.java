package com.acousticdosimeter.soundprocessing;

import android.support.annotation.NonNull;

import com.acousticdosimeter.soundprocessing.interfaces.OnSamplesProcessedCallback;

import java.util.ArrayList;

/**
 * Sound processor used for analysing and filtering read sound samples
 */
public class SoundProcessor {

    // The Google ASR input requirements state that audio input sensitivity
    // should be set such that 90 dB SPL at 1000 Hz yields RMS of 2500 for
    // 16-bit samples, i.e. 20 * log_10(2500 / mGain) = 90.
    // double mGain = 2500.0 / Math.pow(10.0, 90.0 / 20.0);


    /**
     * Instance of filter weighting given array of samples accordingly to A-weighting curve
     */
    private AFilter mAFilter;
    /**
     * ArrayList holding values of next SPL reads for purposes of calculating Leq
     */
    private ArrayList<Double> mMeasuredSPLsForCurrentLeq = new ArrayList<>((int) AudioSettings
            .SPL_READS_PER_LEQ_CALCULATION);
    /**
     * Instance of OnSamplesProcessedCallback where calculated values of SPL and LEQ are passed
     */
    private OnSamplesProcessedCallback mOnSamplesProcessedCallback;
    /**
     * Current calibration
     */
    private volatile int mCalibration;

    /**
     * Constructor
     *
     * @param onSamplesProcessedCallback Instance of callback as this class has no point without it
     */
    public SoundProcessor(@NonNull OnSamplesProcessedCallback onSamplesProcessedCallback) {
        try {
            mAFilter = AFilter.getAFilter(AudioSettings.SAMPLE_RATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mOnSamplesProcessedCallback = onSamplesProcessedCallback;
    }

    /**
     * Method using for setting calibration
     *
     * @param calibration New calibration value
     */
    public void calibrate(int calibration) {
        this.mCalibration = calibration;
    }

    /**
     * Method that calculates SPL value out of given array of samples
     *
     * @param samples Array of samples
     * @param offset  Offset of starting point of samples in an array
     * @param length  Length of read smaples in an array
     * @return Returns SPL value
     */
    private double calculateSPL(@NonNull short[] samples, int offset, int length) {
        // Compute the RMS value.
        double rms = 0;
        for (int i = offset; i < offset + length; i++) {
            rms += samples[i] * samples[i];
        }
        rms = Math.sqrt(rms / length);

        double rmsdB = 20.0 * Math.log10(rms);
        return rmsdB + mCalibration;
    }

    /**
     * Method where subsequent arrays of read samples should be passed
     *
     * @param samples Array of samples
     * @param offset  Offset of starting point of samples in an array
     * @param length  Length of read smaples in an array
     */
    public void analyseSamples(@NonNull short[] samples, int offset, int length) {
        //A-weighting sample
        samples = mAFilter.apply(samples);
        //Calculating SPL value from read samples
        double SPL = calculateSPL(samples, offset, length);
        //Making a callback wih read SPL value
        mOnSamplesProcessedCallback.SPLProcessed(SPL);
        //Adding read SPL to list holding them for Leq processing purpose
        mMeasuredSPLsForCurrentLeq.add(SPL);
        //If number of hold SPL values is greater than spcified calculate Leq
        if (mMeasuredSPLsForCurrentLeq.size() >= AudioSettings.SPL_READS_PER_LEQ_CALCULATION) {
            double Leq = 0;
            for (double i : mMeasuredSPLsForCurrentLeq) {
                Leq += i;
            }
            Leq = Leq / AudioSettings.SPL_READS_PER_LEQ_CALCULATION;
            //Make a callback with calculated Leq value
            mOnSamplesProcessedCallback.LeqProcessed(Leq);
            //Clear list od SPLs so it can be filled again
            mMeasuredSPLsForCurrentLeq.clear();
        }
    }
}
