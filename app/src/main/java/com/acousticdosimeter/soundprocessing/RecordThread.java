package com.acousticdosimeter.soundprocessing;

import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.*;

import com.acousticdosimeter.soundprocessing.interfaces.OnSamplesReadCallback;

/**
 * Thread used to record audio fro devices microphone
 */
public class RecordThread extends Thread {

    private static final String SOUND_RECORD_THREAD_NAME = "SoundRecordThread";

    /**
     * Instance of OnSamplesReadListener where array of read samples should bbe
     * passed after each read
     */
    private OnSamplesReadCallback mOnSamplesReadCallback;
    private Handler processingHandler;

    /**
     * Array of shorts where read samples should be written
     */
    private short[] mSamples = new short[AudioSettings.PERIODIC_READ_SAMPLE_NUMBER * 10];
    /**
     * Flag indicating if recording is in progress
     */
    private volatile boolean recording = false;

    public RecordThread(OnSamplesReadCallback mOnSamplesReadCallback, Handler processingHandler) {
        super(SOUND_RECORD_THREAD_NAME);
        this.mOnSamplesReadCallback = mOnSamplesReadCallback;
        this.processingHandler = processingHandler;
    }

    /**
     * Starts recording
     */
    public void startRecording() {
        // Setup audioRecord

        recording = true;
        start();
    }

    /**
     * Stops recording
     */
    public void stopRecording() {
        recording = false;
        interrupt();
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
        AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, AudioSettings.SAMPLE_RATE,
                AudioSettings.RECORDER_CHANNELS, AudioSettings.RECORDER_AUDIO_ENCODING,
                AudioSettings.SAMPLE_RATE * 10);
        audioRecord.setPositionNotificationPeriod(AudioSettings.PERIODIC_READ_SAMPLE_NUMBER);
        audioRecord.setRecordPositionUpdateListener(mOnRecordPositionUpdateListener,processingHandler);
        while (audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        audioRecord.startRecording();

        int readSamples;
        int samplesToRead;
        while (recording) {
            readSamples = 0;
            // Runs loop till specified number of samples were read
            while (readSamples < AudioSettings.PERIODIC_READ_SAMPLE_NUMBER) {
                // Gets number of samples still to read before specified number
                // is reached
                samplesToRead = AudioSettings.PERIODIC_READ_SAMPLE_NUMBER - readSamples;
                // Read samples, add them to to end of an array and add number
                // of samples read in this iteration to the read samples.
                readSamples += audioRecord.read(mSamples, readSamples, samplesToRead);
            }
        }
        // If recording was stopped release audio record
        if (audioRecord.getState() != AudioRecord.STATE_UNINITIALIZED) {
            if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_STOPPED) {
                audioRecord.stop();
            }
        }
        audioRecord.release();
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
    }

    /**
     * OnRecordPositionUpdateLister, when specified number of samples are read
     * it sends them to instance of OnSamplesReadListener
     */
    AudioRecord.OnRecordPositionUpdateListener mOnRecordPositionUpdateListener = new AudioRecord
            .OnRecordPositionUpdateListener() {
        @Override
        public void onPeriodicNotification(AudioRecord recorder) {
            mOnSamplesReadCallback.samplesRead(mSamples, 0,
                    AudioSettings.PERIODIC_READ_SAMPLE_NUMBER);
        }

        @Override
        public void onMarkerReached(AudioRecord recorder) {
        }
    };
}