package com.acousticdosimeter.soundprocessing.interfaces;

/**
 * Interface that any class wanting to receive readings from SoundProcessor should implement
 */
public interface OnSamplesProcessedCallback {
    void SPLProcessed(double SPL);

    void LeqProcessed(double Leq);
}
