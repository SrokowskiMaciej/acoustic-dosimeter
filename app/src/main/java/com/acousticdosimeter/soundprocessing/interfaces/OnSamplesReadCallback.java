package com.acousticdosimeter.soundprocessing.interfaces;

/**
 * Interface that any class wanting to receive readings from RecordThread should implement
 */
public interface OnSamplesReadCallback {
    void samplesRead(short[] samples, int offset, int length);
}
