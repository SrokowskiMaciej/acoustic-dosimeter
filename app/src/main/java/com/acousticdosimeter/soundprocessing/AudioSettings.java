package com.acousticdosimeter.soundprocessing;

import android.media.AudioFormat;

/**
 * Settings of Audio reader and SPL and Leq calculations
 */
public class AudioSettings {
    public final static int SAMPLE_RATE = 44100;
    public final static int SPL_SAMPLING_FREQUENCY = 8;
    public final static int LEQ_SAMPLING_FREQUENCY = 1;
    public final static double LEQ_READ_PERIOD_SEC = 1 / (float) LEQ_SAMPLING_FREQUENCY;
    public final static double SPL_READS_PER_LEQ_CALCULATION = SPL_SAMPLING_FREQUENCY * LEQ_READ_PERIOD_SEC;
    public final static int PERIODIC_READ_SAMPLE_NUMBER = SAMPLE_RATE / SPL_SAMPLING_FREQUENCY;
    public static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    public static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
}
