package com.acousticdosimeter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Abstract activity that should be extended by all Activities that want to receive broadcasts about current SPL
 * readings
 */
public abstract class SPLDisplayActivity extends AppCompatActivity {

    /**
     * Action for broadcast receiver
     */
    public static final String LEP_SENT_ACTION = "lepSent";
    /**
     * Argument for lep value taken from intent
     */
    public static final String LEP_ARG = "lepArg";

    /**
     * Action for broadcast receiver
     */
    public static final String LEQ_SENT_ACTION = "leqSent";
    /**
     * Argument for lep value taken from intent
     */
    public static final String LEQ_ARG = "leqArg";

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LEP_SENT_ACTION);
        intentFilter.addAction(LEQ_SENT_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mLepBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLepBroadcastReceiver);
    }


    /**
     * Instance of broadcast receiver to get broadcasts sent by Service
     */
    public BroadcastReceiver mLepBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case LEP_SENT_ACTION:
                    if (intent.getExtras().containsKey(LEP_ARG))
                        SPLReady(intent.getDoubleExtra(LEP_ARG, 0.0f));
                    break;
                case LEQ_SENT_ACTION:
                    if (intent.getExtras().containsKey(LEQ_ARG))
                        LEQ1sReady(intent.getDoubleExtra(LEQ_ARG, 0.0f));
                    break;
            }
        }
    };

    abstract protected void SPLReady(double SPL);

    abstract protected void LEQ1sReady(double LEQ);
}
