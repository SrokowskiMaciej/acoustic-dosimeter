package com.acousticdosimeter.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.acousticdosimeter.SPLDisplayActivity;
import com.acousticdosimeter.Utilities.SharePrefs;
import com.acousticdosimeter.soundprocessing.RecordThread;
import com.acousticdosimeter.soundprocessing.SoundProcessor;
import com.acousticdosimeter.soundprocessing.interfaces.OnSamplesProcessedCallback;
import com.acousticdosimeter.soundprocessing.interfaces.OnSamplesReadCallback;

/**
 * Service used for gathering sound levels data
 */
public class RecordService extends Service implements OnSamplesProcessedCallback, OnSamplesReadCallback {

    /**
     * Instance of Sound processor
     */
    private SoundProcessor mSoundProcessor;

    private HandlerThread soundProcessorThread;

    /**
     * Instance of a thread run by ts service to collect sound data from microphone
     */
    private RecordThread mRecordThread;
    /**
     * Wake lock instance
     */
    private PowerManager.WakeLock wakeLock;

    private static final String SOUND_PROCESSOR_THREAD_NAME = "SoundProcessorThread";

    /**
     * Action of a broadcast obtained by Calibration BroadcastReceiver
     */
    public final static String ACTION_CALIBRATION = "actionCalibration";
    /**
     * Argument of a value of calibration obtained by Calibration BroadCastReceiver
     */
    public final static String CALIBRATION_ARG = "calibrationArg";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        //Acquire wakelock to run service even with screen turned off
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "RecordWakelockTag");
        wakeLock.acquire();


        //Initialize sound processor and record thread
        mSoundProcessor = new SoundProcessor(this);
        //Get value of calibration from shared preferences
        SharedPreferences prefs = getSharedPreferences(SharePrefs.SHARED_PREF_PATH, MODE_PRIVATE);
        mSoundProcessor.calibrate(prefs.getInt(SharePrefs.SHARED_PREF_CALIBRATION_ARG, 0));

        soundProcessorThread = new HandlerThread(SOUND_PROCESSOR_THREAD_NAME, android.os.Process
                .THREAD_PRIORITY_FOREGROUND);
        soundProcessorThread.start();

        mRecordThread = new RecordThread(this, new Handler(soundProcessorThread.getLooper()));
        mRecordThread.startRecording();
        //Return START_STICKY so service will run again after it shutdown by the system
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Register broadcast receiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_CALIBRATION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mCalibrationBroadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        mRecordThread.stopRecording();
        mRecordThread = null;
        soundProcessorThread.quit();
        soundProcessorThread=null;
        wakeLock.release();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCalibrationBroadcastReceiver);
        super.onDestroy();
    }

    /**
     * Callback of RecordThread, just pass values to SoundProcessor
     *
     * @param samples Array of samples
     * @param offset  Offset of starting point of samples in an array
     * @param length  Length of read smaples in an array
     */
    @Override
    public void samplesRead(@NonNull short[] samples, int offset, int length) {
        mSoundProcessor.analyseSamples(samples, offset, length);
    }

    /**
     * Callback of SoundProcessor
     *
     * @param SPL Read SPL
     */
    @Override
    public void SPLProcessed(double SPL) {
        //Send broadcast of read SPL sample th whatever wants to hear
        Intent intent = new Intent(SPLDisplayActivity.LEP_SENT_ACTION);
        intent.putExtra(SPLDisplayActivity.LEP_ARG, SPL);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Callback of SoundProcessor
     *
     * @param leq Read Leq
     */
    @Override
    public void LeqProcessed(double leq) {
        //Save read value of Leq with current time
        Intent intent = new Intent(SPLDisplayActivity.LEQ_SENT_ACTION);
        intent.putExtra(SPLDisplayActivity.LEQ_ARG, leq);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Instance of calibration broadcast receiver
     */
    public BroadcastReceiver mCalibrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_CALIBRATION)) {
                if (intent.getExtras().containsKey(CALIBRATION_ARG))
                    mSoundProcessor.calibrate(intent.getIntExtra(CALIBRATION_ARG, 0));
            }
        }
    };


}
